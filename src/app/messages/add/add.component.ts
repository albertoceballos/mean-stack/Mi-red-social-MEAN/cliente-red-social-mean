import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//url
import { GLOBAL } from '../../services/global';

//servicios
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';

//modelos
import { Message } from '../../models/Message';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  providers: [MessageService, UserService, FollowService],
})
export class AddComponent implements OnInit {

  public url;
  public title: string;
  public message: Message;
  public identity;
  public token;
  public status: string;
  public follows;

  constructor(private _messageService: MessageService,
    private _userService: UserService,
    private _followService: FollowService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    this.title = "Enviar mensajes";
    this.url=GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.message = new Message('', '', '', '', this.identity._id, '');
  }

  ngOnInit() {
    console.log("add component cargado");
    this.getMyFollows();
  }



  onSubmit(form) {
    console.log(this.message);
    this._messageService.addMessage(this.token, this.message).subscribe(
      response => {
        if (response.message) {
          console.log(response.message);
          this.status = "success";
          setTimeout(()=>{
            this.status="";
          },2000);
        } else {
          console.log("error");
          this.status = "error";
          form.reset();
        }
      },
      error => {
        let errorMessage = error;
        if (errorMessage != null) {
          console.log(errorMessage);
          this.status = "error";
        }
      }
    );
  }


  getMyFollows() {
    this._followService.getMyFollows(this.token).subscribe(
      response => {
        this.follows = response.follows;
        console.log(this.follows);
      },
      error => {
        console.log(error);
      }
    );
  }

  


}
