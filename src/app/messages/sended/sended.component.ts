import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//url
import { GLOBAL } from '../../services/global';

//servicios
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';

//modelos
import { Message } from '../../models/Message';

@Component({
  selector: 'sended',
  templateUrl: './sended.component.html',
  providers:[MessageService,UserService,FollowService],
})
export class SendedComponent implements OnInit, DoCheck {

  public title:string;
  public url;
  public message: Message;
  public identity;
  public token;
  public status: string;
  public messages:Message[];
  public page;
  public pages;
  public total;
  public prevPage;
  public nextPage;
  public arrayPages;


  constructor(private _messageService: MessageService,
    private _userService: UserService,
    private _followService: FollowService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) 
    {
    this.title="Mensajes enviados";
    this.url=GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.arrayPages=[];
    this.page=1;

   }

  ngOnInit() {
    console.log("Sended component cargado");
    this.actualPage();
  }

  ngDoCheck(){
    
  }
  actualPage() {

    this._activatedRoute.params.subscribe(params => {
      let page = parseInt(params['page']);
      this.page = page;
      if (!page) {
        page = 1;
        this.nextPage = page + 1;
      } else {
        this.nextPage = page + 1;
        this.prevPage = page - 1;
        if (this.prevPage <= 0) {
          this.prevPage = 1;
        }
      }

      this.getEmittedMessages(this.token,this.page);
    });
  }

  getEmittedMessages(token,page){
    this._messageService.getEmittedMessages(token,page).subscribe(
      response=>{
        if(response.messages){
          this.messages=response.messages;
          console.log(response);
          this.status="success";
          this.page=response.page;
          this.pages=response.pages;
          this.total=response.total;
          this.arrayPages = [];
          for (let i = 1; i <= this.pages; i++) {
            this.arrayPages.push(i);
          }
        }else{
          this.status="error";
        }
      },
      error=>{
        console.log(<any>error);
        this.status="error";
      }
    );
  }

}