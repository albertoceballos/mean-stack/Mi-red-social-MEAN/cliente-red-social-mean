import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//servicios

import { UserService } from '../../services/user.service'
import { FollowService } from '../../services/follow.service';

//modelos
import { User } from '../../models/User';
import { Follow } from '../../models/Follow';

import { GLOBAL } from '../../services/global';


@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService, FollowService],
})
export class ProfileComponent implements OnInit {
  public title: string;
  public url: string;
  public user: User;
  public token;
  public identity;
  public stats;
  public status: string;
  public followed;
  public following;


  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _followService: FollowService) {

    this.title = "Perfil de usuario";
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.followed=false;
    this.following=false;
  }

  ngOnInit() {
    console.log("Componente profile cargado");
    this.pageLoad();
  }

  pageLoad(){
    this._activatedRoute.params.subscribe(params=>{
      let userId=params['id'];
      this.getUser(userId);
      this.getCounters(userId);
    });
  }

  getUser(id){
    this._userService.getUser(id).subscribe(
      response=>{
        if(response.user){
          this.user=response.user;

          if(response.followed && response.followed._id){
            this.followed=true;
          }else{
            this.followed=false;
          }

          if(response.following && response.following._id){
            this.following=true;
          }else{
            this.following=false;
          }

          
        }else{
          this.status='error';
        }

      },
      error=>{
        let errorMessage=error;
        if(errorMessage!=null){
          console.log(errorMessage);
          this.status="error";
          this._router.navigate(['/profile',this.identity._id]);
        }
      }
    );
  }

  getCounters(id){
    this._userService.getCounters(id).subscribe(
      response=>{
        this.stats=response;
        console.log(this.stats);
      },
      error=>{
        let errorMessage=error;
        if(errorMessage!=null){
          console.log(errorMessage);
          this.status="error";
        }
      }
    );
  }

  followUser(followed){
    var follow=new Follow('',this.identity,followed);

    this._followService.addFollow(this.token,follow).subscribe(
      response=>{
        this.following=true;
      },
      error=>{
        console.log(<any>error);
      }
    );
  }


  unfollowUser(followed){
    this._followService.deleteFollow(this.token,followed).subscribe(
      response=>{
        this.following=false;
      },  
      error=>{
        console.log(<any>error);
      }
    );
  }

  public followUserOver;
  mouseEnter(id){
    this.followUserOver=id;
  }

  mouseLeave(){
    this.followUserOver=0;
  }
}
