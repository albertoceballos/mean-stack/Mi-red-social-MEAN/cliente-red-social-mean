import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

//servicios
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';

//modelos
import { Publication } from '../../models/Publication';

//url
import { GLOBAL } from '../../services/global';
import * as $ from 'jquery';



@Component({
  selector: 'timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
  providers: [UserService, PublicationService],
})
export class TimelineComponent implements OnInit {
  public title: string;
  public url: string;
  public identity;
  public token;
  public status:string;
  public publications: Publication[];
  public page;
  public pages;
  public total;
  public noMore=false;

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _pubsService: PublicationService) {
    this.title = "Timeline";
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.page=1;
    this.status="";

  }

  ngOnInit() {
    console.log("Componente Timeline cargado");
    this.getPublications(this.page);
  }

  getPublications(page,adding=false) {
    this._pubsService.getPublications(this.token,page).subscribe(
      response=>{
        console.log(response);
        if(response.publications){
          this.pages=response.pages;
          this.total=response.total;

          if(adding==false){
            this.publications=response.publications;
          }else{
            //si se pulsa el botón de ver más,concateno las publicaciones que ya tengo con las siguientes
            let arrayPubsA=this.publications;
            let arrayPubsB=response.publications;
            this.publications=arrayPubsA.concat(arrayPubsB);

            //efecto de scroll con Jquery
            $('html,body').animate({scrollTop:$('body').prop('scrollHeight')},1000);

            //si el array es igual que el total de publicaciones me pone el noMore a true para ocultar el botón:
            if(this.publications.length==this.total){
              this.noMore=true;
            }
          }

        }else{
          this.status='error';
        }
      },
      error=>{
        let errorMessage=error;
        console.log(errorMessage);
        this.status="error";
      }
    );
  }

 
  viewMore(){
   
    this.page+=1;
    this.getPublications(this.page,true);
  }


  refresh(event=null){
    this.getPublications(1);
  }


  openCloseImage(id,event){
    if(event.target.classList.contains("fa-times-circle")){
      document.querySelector("#image"+id).setAttribute("style","display:none");
      event.target.classList.remove("fa-times-circle");
      event.target.classList.add("fa-image");
      event.target.setAttribute("style","color:steelblue;font-size:2.5em");
    }else{
      document.querySelector("#image"+id).setAttribute("style","display:inline");
      event.target.classList.add("fa-times-circle");
      event.target.classList.remove("fa-image");
      event.target.setAttribute("style","color:red;font-size:1.2em");
    }
  }

  deletePublication(idPub){
    this._pubsService.deletePublication(this.token,idPub).subscribe(
      response=>{
        this.refresh();
        console.log(response);
      },
      error=>{
        let errorMessage=<any>error;
        if(errorMessage!=null){
          console.log(errorMessage);
        }
      }
    );
  }

}
