import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

//servicios
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';

//modelos
import { Publication } from '../../models/Publication';

//url
import { GLOBAL } from '../../services/global';
import * as $ from 'jquery';



@Component({
  selector: 'publication',
  templateUrl: './publication.component.html',
  styleUrls: [],
  providers: [UserService, PublicationService],
})
export class PublicationComponent implements OnInit {
  public title: string;
  public url: string;
  public identity;
  public token;
  public status:string;
  public publications: Publication[];
  public page;
  public pages;
  public total;
  public noMore=false;

  @Input() user:string;

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _pubsService: PublicationService) {
    this.title = "Timeline";
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.page=1;
    this.status="";
    
  }

  ngOnInit() {
    console.log("Componente Timeline cargado");
    this.getPublications(this.user,this.page);
  }

  getPublications(user,page,adding=false) {
    this._pubsService.getPublicationsUser(this.token,user,page).subscribe(
      response=>{
        if(response.publications){
          this.pages=response.pages;
          this.total=response.total;

          if(this.pages==1){
            this.noMore=true;
          }

          if(adding==false){
            this.publications=response.publications;
          }else{
            //si se pulsa el botón de ver más,concateno las publicaciones que ya tengo con las siguientes
            let arrayPubsA=this.publications;
            let arrayPubsB=response.publications;
            this.publications=arrayPubsA.concat(arrayPubsB);

            //efecto de scroll con Jquery
            $('html,body').animate({scrollTop:$('html').prop('scrollHeight')},1000);

            //si el array es igual que el total de publicaciones me pone el noMore a true para ocultar el botón:
            if(this.publications.length==this.total){
              this.noMore=true;
            }
           
          }

        }else{
          this.status='error';
        }
      },
      error=>{
        let errorMessage=error;
        console.log(errorMessage);
        this.status="error";
      }
    );
  }

 
  viewMore(){
   
    this.page+=1;
    if(this.page==this.total){
      this.noMore=true;
    }
    this.getPublications(this.user,this.page,true);
  }


  refresh(event){
    this.getPublications(this.user,1);
  }


}
