export class Publication{
    public _id:string;
    public text:string;
    public file:string;
    public created_at:string;
    public user:string;

    constructor(id,text,file,created,user){
        this._id=id;
        this.text=text;
        this.file=file;
        this.created_at=created;
        this.user=user;
    }

}