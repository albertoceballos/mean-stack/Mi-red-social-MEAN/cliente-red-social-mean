
export class User{
    public _id:string;
    public name:string;
    public surname:string;
    public nick:string;
    public email:string;
    public password:string;
    public role:string;
    public image:string;
    public getToken:string;


    constructor(id,name,surname,nick,email,password,role,image,token){
        this._id=id;
        this.name=name;
        this.surname=surname;
        this.nick=nick;
        this.email=email;
        this.password=password;
        this.role=role;
        this.image=image;
        this.getToken=token;
    }
}
